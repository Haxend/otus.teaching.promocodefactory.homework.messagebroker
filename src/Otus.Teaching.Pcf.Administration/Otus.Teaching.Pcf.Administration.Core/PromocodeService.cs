﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core
{
    public class AdministrationPromocodeService
    {
        private readonly IRepository<Employee> _employeeRepository;

        public AdministrationPromocodeService(IRepository<Employee> employeeRepository)
        {
            this._employeeRepository = employeeRepository;
        }

        public async Task<bool> UpdateApplliedPromocode(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return false;

            employee.AppliedPromocodesCount++;

            await _employeeRepository.UpdateAsync(employee);

            return true;
        }
    }
}
