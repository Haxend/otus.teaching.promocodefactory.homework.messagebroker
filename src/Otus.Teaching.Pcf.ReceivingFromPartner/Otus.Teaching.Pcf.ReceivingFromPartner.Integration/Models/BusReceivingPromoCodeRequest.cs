﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Models
{
    public class BusReceivingPromoCodeRequest
    {
        public Guid Id { get; set; }
        public ReceivingPromoCodeRequest Request { get; set; }
    }
}
