﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Exceptions
{
    public class BadRequestException : Exception
    {
        public BadRequestException(string error)
            : base(error)
        {
            
        }
    }
}
