﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Exceptions;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Mappers;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Services
{
    public class PromocodeService
    {
        private readonly IRepository<Partner> _partnersRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly INotificationGateway _notificationGateway;
        private readonly IGivingPromoCodeToCustomerGateway _givingPromoCodeToCustomerGateway;
        private readonly IAdministrationGateway _administrationGateway;

        public PromocodeService(IRepository<Partner> partnersRepository,
            IRepository<Preference> preferencesRepository,
            INotificationGateway notificationGateway,
            IGivingPromoCodeToCustomerGateway givingPromoCodeToCustomerGateway,
            IAdministrationGateway administrationGateway)
        {
            _partnersRepository = partnersRepository;
            _preferencesRepository = preferencesRepository;
            _notificationGateway = notificationGateway;
            _givingPromoCodeToCustomerGateway = givingPromoCodeToCustomerGateway;
            _administrationGateway = administrationGateway;
        }

        public async Task<PartnerPromocode> ReceivePromoCodeFromPartnerWithPreferenceAsync(Guid id,
            ReceivingPromoCodeRequest request)
        {
            var partner = await _partnersRepository.GetByIdAsync(id);

            if (partner == null)
            {
                throw new BadRequestException("Партнер не найден");
            }

            var activeLimit = partner.PartnerLimits.FirstOrDefault(x
                => !x.CancelDate.HasValue && x.EndDate > DateTime.Now);

            if (activeLimit == null)
            {
                throw new BadRequestException("Нет доступного лимита на предоставление промокодов");
            }

            if (partner.NumberIssuedPromoCodes + 1 > activeLimit.Limit)
            {
                throw new BadRequestException("Лимит на выдачу промокодов превышен");
            }

            if (partner.PromoCodes.Any(x => x.Code == request.PromoCode))
            {
                throw new BadRequestException("Данный промокод уже был выдан ранее");
            }

            //Получаем предпочтение по имени
            var preference = await _preferencesRepository.GetByIdAsync(request.PreferenceId);

            if (preference == null)
            {
                throw new BadRequestException("Предпочтение не найдено");
            }

            PromoCode promoCode = PromoCodeMapper.MapFromModel(request, preference, partner);
            partner.PromoCodes.Add(promoCode);
            partner.NumberIssuedPromoCodes++;

            await _partnersRepository.UpdateAsync(partner);

            //TODO: Чтобы информация о том, что промокод был выдан парнером была отправлена
            //в микросервис рассылки клиентам нужно либо вызвать его API, либо отправить событие в очередь
            await _givingPromoCodeToCustomerGateway.GivePromoCodeToCustomer(promoCode);

            //TODO: Чтобы информация о том, что промокод был выдан парнером была отправлена
            //в микросервис администрирования нужно либо вызвать его API, либо отправить событие в очередь

            if (request.PartnerManagerId.HasValue)
            {
                await _administrationGateway.NotifyAdminAboutPartnerManagerPromoCode(request.PartnerManagerId.Value);
            }

            return new PartnerPromocode { Partner = partner, Promocode = promoCode };
        }
    }
}
