﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models
{
    public class NotifyTransaction
    {
        public string CardNumber { get; set; } = null!;
        public decimal Amount { get; set; }
    }
}
