﻿namespace Shared.Models
{
    public class NotifyAdminAboutPartnerManagerPromoDto
    {
        public Guid PartnerManagerId { get; set;}
    }
}
